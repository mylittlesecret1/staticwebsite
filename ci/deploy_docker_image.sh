#!/usr/bin/env bash
set -euo pipefail

if [ -z "${IMAGE_NAME:-}" ]; then
    # shellcheck disable=SC2155
    echo "Image name is not set"
    exit 1
fi

docker login azure
docker context use myLittleSecretAciContext
docker compose up
