MIN_MAKE_VERSION := 3.82

ifneq ($(MIN_MAKE_VERSION),$(firstword $(sort $(MAKE_VERSION) $(MIN_MAKE_VERSION))))
$(error GNU Make $(MIN_MAKE_VERSION) or higher required)
endif

.DEFAULT_GOAL:=help

##@ build
.PHONY: lint
build: ## builds docker image
	export IMAGE_NAME="staticweb_local_build" && ./ci/build_docker_image.sh
up: ## runs docker image and expose ports 80 a 443
	export IMAGE_NAME="staticweb_local_build" && docker-compose up

##@ development
.PHONY: push
deploy: ## pushes docker image to docker hub
	./ci/deploy_docker_image.sh

##@ Lint
.PHONY: lint lint-conventions lint-shellcheck lint-yamllint lint-hadolint lint-golint

lint: lint-conventions lint-shellcheck lint-yamllint lint-hadolint lint-typescript ## Run all linters

lint-conventions: ## Run Conventions Checker
	@ci/lint/conventions.sh

lint-shellcheck: ## Run ShellCheck
	@ci/lint/shellcheck.sh

lint-yamllint: ## Run YAML Lint
	@ci/lint/yamllint.sh

lint-hadolint: ## Run Dockerfile Lint
	@ci/lint/hadolint.sh
lint-typescript: ## Run ESLINT against the whole code base
	docker-compose run --rm app npm run lint
lint-fix: ## Run ESLINT against the whole code base with fix flag
	docker-compose run --rm admintool-api npm run lint:fix

##@ Misc
swagger: ## opens swagger ui
	 xdg-open http://127.0.0.1:1337/docs/
init_repo: ## init repo
	git submodule update --init --force --recursive

.PHONY: help
help:
	@awk 'BEGIN {FS = ":.*##"; printf "Usage: \033[36mmake \033[96m<target>\033[0m\n"} /^[a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-25s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)
