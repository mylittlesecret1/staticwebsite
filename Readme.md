## Deploy to Azure

start with docker login
```
docker login azure
```

for login from CLI script we have to create application in Azure (App registrations) and get client_id and client_secret
together with tenant_id

```

creating ACI context (Azure Container Instance), this will create a context in your docker config
```
docker context create aci myLittleSecretAciContext
```

use the context
```
docker context use myLittleSecretAciContext
```

## Deploy to ACI
```
docker run --rm -d -p 80:80 --name mycontainer --context myLittleSecretAksContext azurecr.io/mycontainer:latest
```

## Deploy to ACI with docker compose
```
docker compose up
```

Switch to ACI context
```
docker context use myLittleSecretAciContext
```
Switch to Default context
```
docker context use default
```

List all contexts
```
docker context ls
```

Release a new version of the container

first login to the registry
```
docker login registry.gitlab.com/mylittlesecret1/staticwebsite
```

and then run docker compose up
```
docker compose up
```

